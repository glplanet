// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#version 150

in vec3 cartesian_position;
in vec2 vertex_geographic_position;
in vec2 vertex_texture_coordinate;

uniform mat4 mvp;

out vec3 model_cartesian_position;
out vec2 texture_coordinate;

void main() {
  gl_Position = mvp * vec4(cartesian_position, 1.0);
  model_cartesian_position = cartesian_position;
  texture_coordinate = vertex_texture_coordinate;
}
