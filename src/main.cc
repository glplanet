// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include <getopt.h>
#include <locale.h>

#include <iostream>

#include "src/glplanet.h"
#include "src/x/init.h"
#include "third_party/abseil/absl/strings/numbers.h"
#include "third_party/abseil/absl/strings/string_view.h"

namespace {

constexpr absl::string_view kShortUsage = "Usage: glplanet [OPTION...]\n";

constexpr absl::string_view kHelp = R"(
Render a 3D globe lit in real time.

Options:
      --help                  display this help and exit
      --longitude=DEGREES     center globe on this longitude, in degrees east
      --latitude=DEGREES      center globe on this latitude, in degrees north
      --version               display version and license information, and exit
)";

constexpr absl::string_view kAskForHelp =
    "Try 'glplanet --help' for more information.\n";

constexpr absl::string_view kVersionInfo = R"(glplanet development build
Copyright 2021, 2022 Benjamin Barenblat
Licensed under the Apache License, Version 2.0

glplanet incorporates Eigen, whose source is governed by the terms of the
Mozilla Public License, Version 2.0. Eigen's source is available at
https://deb.debian.org/debian/pool/main/e/eigen3/eigen3_3.3.9.orig.tar.bz2.
)";

enum {
  kHelpLongOption = 128,
  kLongitudeLongOption,
  kLatitudeLongOption,
  kVersionLongOption,
};

}  // namespace

int main(int argc, char* argv[]) {
  setlocale(LC_ALL, "");
  x::Initialize();

  glplanet::MainOptions glplanet_options;

  static option long_options[] = {
      {"help", no_argument, nullptr, kHelpLongOption},
      {"longitude", required_argument, nullptr, kLongitudeLongOption},
      {"latitude", required_argument, nullptr, kLatitudeLongOption},
      {"version", no_argument, nullptr, kVersionLongOption},
      {nullptr, 0, nullptr, 0},
  };
  while (true) {
    int c = getopt_long(argc, argv, "", long_options, /*longindex=*/nullptr);
    if (c == -1) {
      break;
    }
    switch (c) {
      case kLongitudeLongOption:
        if (!absl::SimpleAtod(optarg, &glplanet_options.longitude_degrees)) {
          std::cerr << "glplanet: invalid longitude " << optarg << '\n';
          return 1;
        }
        break;

      case kLatitudeLongOption:
        if (!absl::SimpleAtod(optarg, &glplanet_options.latitude_degrees)) {
          std::cerr << "glplanet: invalid latitude " << optarg << '\n';
          return 1;
        }
        break;

      case kHelpLongOption:
        std::cout << kShortUsage << kHelp;
        return 0;
      case kVersionLongOption:
        std::cout << kVersionInfo;
        return 0;
      case '?':
        std::cerr << kAskForHelp;
        return 1;
      default:
        std::cerr << "Internal error; please report.\n";
        return 1;
    }
  }
  if (optind != argc) {
    std::cerr << kShortUsage << kAskForHelp;
    return 1;
  }

  return glplanet::Main(glplanet_options);
}
