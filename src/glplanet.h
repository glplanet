// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#ifndef GLPLANET_SRC_GLPLANET_H_
#define GLPLANET_SRC_GLPLANET_H_

namespace glplanet {

struct MainOptions {
  // The part of the Earth under the camera, in degrees east longitude and north
  // latitude.
  double longitude_degrees = 0.0;
  double latitude_degrees = 0.0;
};

int Main(const MainOptions&);

}  // namespace glplanet

#endif  // GLPLANET_SRC_GLPLANET_H_
