// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#version 150

// The color of sunlight (5800 K) in sRGB.
const vec4 kSunColor = vec4(1.0, 0.9411765, 0.9137255, 1.0);

in vec3 model_cartesian_position;  // unit vector pointing toward current point
in vec2 texture_coordinate;

uniform sampler2D planet;
uniform sampler2D clouds;
uniform vec3 sun_direction;  // unit vector pointing toward sun

out vec4 out_color;

void main() {
  // Compute diffuse illumination from the Sun. We assume all rays are parallel,
  // which isn't true, but at Earth scales, it causes a maximum error of about
  // 0.003 degrees, well within our error budget.
  float diffuse = max(dot(model_cartesian_position, sun_direction), 0.0);

  // The planet is a mixture of the ground texture and the cloud texture. Assume
  // clouds reflect sunlight; the ground texture needs no color adjustment,
  // since it's already colored as if being hit by sunlight.
  out_color = diffuse * mix(texture(planet, texture_coordinate), kSunColor,
                            texture(clouds, texture_coordinate).r);
}
