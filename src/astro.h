// Copyright 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#ifndef GLPLANET_SRC_ASTRO_H_
#define GLPLANET_SRC_ASTRO_H_

#include <chrono>
#include <utility>

#include "third_party/date/include/date/tz.h"

namespace glplanet {

// The apparent right ascension and declination of the sun, in radians. Accurate
// to 0.01 degrees.
std::pair<double, double> SunEquatorialPosition(
    std::chrono::time_point<date::tai_clock,
                            std::chrono::milliseconds>) noexcept;

// The apparent sidereal time at Greenwich, in radians.
double ApparentSiderealTimeAtGreenwich(
    std::chrono::time_point<date::tai_clock,
                            std::chrono::milliseconds>) noexcept;

// The east longitude and north latitude of the location observing a Sun transit
// (i.e., the location at which it's high noon), in radians.
std::pair<double, double> HighNoonLocation(
    std::chrono::time_point<date::tai_clock,
                            std::chrono::milliseconds>) noexcept;

}  // namespace glplanet

#endif  // GLPLANET_SRC_ASTRO_H_
