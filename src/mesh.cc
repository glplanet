// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/mesh.h"

#include <math.h>

#include <string>
#include <vector>

#include "src/util.h"
#include "third_party/abseil/absl/strings/substitute.h"

namespace glplanet {

std::string UvSphere::Coordinates::DebugString() const noexcept {
  return absl::Substitute(
      "Coordinates{.x = $0, .y = $1, .z = $2, .u = $3, .v = $4}", x, y, z, u,
      v);
}

UvSphere::UvSphere(int sectors_per_turn, int slices) noexcept {
  DCHECK(sectors_per_turn >= 3);
  DCHECK(slices >= 2);

  int sectors_per_slice = sectors_per_turn + 1;

  for (int j = 0; j <= slices; ++j) {
    for (int i = 0; i < sectors_per_slice; ++i) {
      double longitude = i * (2.0 * M_PI / sectors_per_turn) - M_PI;
      double latitude = j * (M_PI / slices) - M_PI / 2.0;
      vertices.push_back({
          .x = static_cast<float>(cos(latitude) * cos(longitude)),
          .y = static_cast<float>(cos(latitude) * sin(longitude)),
          .z = sinf(latitude),

          .u = static_cast<float>(i / static_cast<double>(sectors_per_turn)),
          .v = static_cast<float>(1 - j / static_cast<double>(slices)),
      });
    }
  }

  // Generate triangle elements.
  for (int j = 0; j < slices; ++j) {
    for (int i = 0; i < sectors_per_turn; ++i) {
      uint32_t ll = j * sectors_per_slice + i;
      uint32_t lr = ll + 1;
      uint32_t ul = ll + sectors_per_slice;
      uint32_t ur = ul + 1;

      DCHECK(ll < vertices.size());
      DCHECK(lr < vertices.size());
      DCHECK(ul < vertices.size());
      DCHECK(ur < vertices.size());

      elements.insert(elements.end(), {ll, ur, ul, lr, ur, ll});
    }
  }
}

}  // namespace glplanet
