// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/x/event.h"

#include <stdint.h>
#include <string.h>
#include <xcb/xcb.h>

#include <algorithm>
#include <array>
#include <stdexcept>

#include "third_party/abseil/absl/types/variant.h"

namespace x {

namespace {

constexpr uint8_t kXcbResponseTypeMask = 0x7f;

}  // namespace

Event FromXcbGenericEvent(const xcb_generic_event_t& event) noexcept {
  switch (event.response_type & kXcbResponseTypeMask) {
    case XCB_EXPOSE:
      return ExposeEvent(event);
    case XCB_CONFIGURE_NOTIFY:
      return ConfigureNotifyEvent(event);
    case XCB_CLIENT_MESSAGE:
      return ClientMessageEvent(event);
    default:
      return UnknownEvent(event);
  }
}

std::array<char, 32> SerializeEvent(const Event& event) noexcept {
  std::array<char, 32> raw = {};
  absl::visit(
      [&](auto&& ev) {
        const auto& xcb_ev = ev.AsXcbEvent();
        memcpy(raw.data(), &xcb_ev, std::min(raw.size(), sizeof(xcb_ev)));
      },
      event);
  return raw;
}

}  // namespace x
