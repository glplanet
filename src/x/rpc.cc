// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/x/rpc.h"

#include <xcb/xcb.h>

#include <memory>

#include "src/x/error.h"
#include "src/x/types.h"
#include "src/x/util.h"

namespace x {

namespace {

template <typename T>
using ManagedPtr = std::unique_ptr<T, x_internal::FreeDeleter>;

}  // namespace

void VoidCompletion::Check() const&& {
  auto error =
      ManagedPtr<xcb_generic_error_t>(xcb_request_check(xcb_, cookie_));
  if (error != nullptr) {
    throw Error(error->error_code);
  }
}

Id InternAtomCompletion::Get() const&& {
  xcb_generic_error_t* error_raw;
  auto reply = ManagedPtr<xcb_intern_atom_reply_t>(
      xcb_intern_atom_reply(xcb_, cookie_, &error_raw));
  if (reply == nullptr) {
    auto error = ManagedPtr<xcb_generic_error_t>(error_raw);
    throw Error(error->error_code);
  }
  return reply->atom;
}

}  // namespace x
