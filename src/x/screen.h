// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#ifndef GLPLANET_SRC_X_SCREEN_H_
#define GLPLANET_SRC_X_SCREEN_H_

#include <stdint.h>
#include <xcb/xcb.h>
#include <xcb/xcb_aux.h>

#include "src/x/types.h"

namespace x {

// An X screen.
//
// This class is thread-safe.
class Screen final {
 public:
  explicit Screen(xcb_screen_t* screen) noexcept : screen_(screen) {}

  Screen(Screen&&) noexcept = default;
  Screen& operator=(Screen&&) noexcept = default;

  Id root() const noexcept { return screen_->root; }
  Id black_pixel() const noexcept { return screen_->black_pixel; }

  uint8_t DepthOfVisual(Id visual) const noexcept {
    return xcb_aux_get_depth_of_visual(screen_, visual);
  }

  xcb_screen_t* AsXcbScreen() const noexcept { return screen_; }

 private:
  xcb_screen_t* screen_;
};

}  // namespace x

#endif  // GLPLANET_SRC_X_SCREEN_H_
