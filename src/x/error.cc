// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/x/error.h"

#include <stdint.h>
#include <xcb/xcb.h>

#include <stdexcept>

#include "src/util.h"
#include "third_party/abseil/absl/strings/str_cat.h"

namespace x {

namespace {

const char* StatusCodeName(uint8_t code) noexcept {
  switch (code) {
    case XCB_REQUEST:
      return "Request";
    case XCB_VALUE:
      return "Value";
    case XCB_WINDOW:
      return "Window";
    case XCB_PIXMAP:
      return "Pixmap";
    case XCB_ATOM:
      return "Atom";
    case XCB_CURSOR:
      return "Cursor";
    case XCB_FONT:
      return "Font";
    case XCB_MATCH:
      return "Match";
    case XCB_DRAWABLE:
      return "Drawable";
    case XCB_ACCESS:
      return "Access";
    case XCB_ALLOC:
      return "Alloc";
    case XCB_COLORMAP:
      return "Colormap";
    case XCB_G_CONTEXT:
      return "XCB_Context";
    case XCB_ID_CHOICE:
      return "XCB_Choice";
    case XCB_NAME:
      return "Name";
    case XCB_LENGTH:
      return "Length";
    case XCB_IMPLEMENTATION:
      return "Implementation";
    default:
      DCHECK(false);
      return "unknown error";
  }
}

}  // namespace

Error::Error(uint8_t code) noexcept
    : std::runtime_error(absl::StrCat("X error: ", StatusCodeName(code))) {}

}  // namespace x
