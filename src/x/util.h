// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#ifndef GLPLANET_SRC_X_UTIL_H_
#define GLPLANET_SRC_X_UTIL_H_

#include <stdlib.h>

namespace x_internal {

// A deleter for std::unique_ptr that `free`s instead of calling `delete`.
class FreeDeleter final {
 public:
  template <typename T>
  void operator()(T* p) noexcept {
    free(p);
  }
};

}  // namespace x_internal

#endif  // GLPLANET_SRC_X_UTIL_H_
