// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// In-flight RPCs.
//
// Each of these classes represents an individual X RPC, possibly still in
// flight. Inspecting an instance will block until the RPC has completed.

#ifndef GLPLANET_SRC_X_RPC_H_
#define GLPLANET_SRC_X_RPC_H_

#include <xcb/xcb.h>

#include "src/x/types.h"

namespace x {

// An RPC that returns nothing.
//
// This class is thread-safe.
class VoidCompletion final {
 public:
  explicit VoidCompletion(xcb_connection_t* xcb,
                          xcb_void_cookie_t cookie) noexcept
      : xcb_(xcb), cookie_(cookie) {}

  VoidCompletion(const VoidCompletion&) noexcept = default;
  VoidCompletion& operator=(const VoidCompletion&) noexcept = default;
  VoidCompletion(VoidCompletion&&) noexcept = default;
  VoidCompletion& operator=(VoidCompletion&&) noexcept = default;

  // Blocks until the RPC completes and checks to make sure it was successful.
  // Throws Error if it was not.
  void Check() const&&;

 private:
  xcb_connection_t* xcb_;
  xcb_void_cookie_t cookie_;
};

// An InternAtom RPC.
//
// This class is thread-safe.
class InternAtomCompletion final {
 public:
  explicit InternAtomCompletion(xcb_connection_t* xcb,
                                xcb_intern_atom_cookie_t cookie) noexcept
      : xcb_(xcb), cookie_(cookie) {}

  InternAtomCompletion(const InternAtomCompletion&) noexcept = default;
  InternAtomCompletion& operator=(const InternAtomCompletion&) noexcept =
      default;
  InternAtomCompletion(InternAtomCompletion&&) noexcept = default;
  InternAtomCompletion& operator=(InternAtomCompletion&&) noexcept = default;

  // Blocks until the RPC completes, checks to make sure it was successful, and
  // returns the atom. Throws Error if the RPC failed.
  Id Get() const&&;

 private:
  xcb_connection_t* xcb_;
  xcb_intern_atom_cookie_t cookie_;
};

}  // namespace x

#endif  // GLPLANET_SRC_X_RPC_H_
