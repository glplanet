// Copyright 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/astro.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <chrono>

#include "third_party/date/include/date/tz.h"

namespace glplanet {
namespace {

using ::testing::DoubleNear;
using ::testing::Pair;

TEST(SunEquatorialPositionTest, MeeusExercise25A) {
  // October 13, 1992, at 00:00:00 TDT (T = -0.072'183'436)
  constexpr auto t =
      std::chrono::time_point<date::tai_clock, std::chrono::milliseconds>() +
      std::chrono::days(34 * 365 + 8 /* leap days */) +
      std::chrono::days(31 + 29 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 11) +
      std::chrono::hours(23) + std::chrono::minutes(59) +
      std::chrono::milliseconds(27'816);
  EXPECT_THAT(SunEquatorialPosition(t), Pair(DoubleNear(-2.82078673, 1e-8),
                                             DoubleNear(-0.1358751, 1e-8)));
}

TEST(ApparentSiderealTimeAtGreenwichTest, MeeusExercise12A) {
  // April 10, 1987, at 00:00:00 UT (T = -0.127'296'372'348)
  constexpr auto t =
      std::chrono::time_point<date::tai_clock, std::chrono::milliseconds>() +
      std::chrono::days(29 * 365 + 7 /* leap days */) +
      std::chrono::days(31 + 28 + 31 + 8) + std::chrono::hours(23) +
      std::chrono::minutes(59) + std::chrono::milliseconds(27'816);
  EXPECT_THAT(ApparentSiderealTimeAtGreenwich(t), DoubleNear(-2.832805, 1e-6));
}

TEST(HighNoonLocationTest, MarchEquinox) {
  // March 20, 2022 15:29:52.331 UT
  constexpr auto t =
      std::chrono::time_point<date::tai_clock, std::chrono::milliseconds>() +
      std::chrono::days(64 * 365 + 16 /* leap days */) +
      std::chrono::days(31 + 28 + 19) + std::chrono::hours(15) +
      std::chrono::minutes(29) + std::chrono::milliseconds(24'515);
  EXPECT_THAT(HighNoonLocation(t),
              Pair(DoubleNear(-0.883655431852, 1e-10), DoubleNear(0.0, 1e-10)));
}

}  // namespace
}  // namespace glplanet
