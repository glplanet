// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#ifndef GLPLANET_SRC_SCENE_H_
#define GLPLANET_SRC_SCENE_H_

#include <stdint.h>

#include <Eigen/Core>
#include <chrono>

#include "src/gl/buffer.h"
#include "src/gl/draw.h"
#include "src/gl/shader.h"
#include "src/gl/texture.h"
#include "src/gl/vertex_array.h"
#include "third_party/abseil/absl/base/attributes.h"
#include "third_party/date/include/date/date.h"
#include "third_party/date/include/date/tz.h"

namespace glplanet {

// The scene to draw.
//
// This class hangs onto some OpenGL state, so it is thread-hostile.
class Scene final {
 public:
  struct Options {
    // The part of the Earth under the camera, in radians east longitude and
    // north latitude.
    double longitude_radians;
    double latitude_radians;
  };

  // Constructs a scene. There must be an OpenGL context bound to the current
  // thread.
  explicit Scene(const Options&);

  Scene(Scene&&) noexcept = default;
  Scene& operator=(Scene&&) noexcept = default;

  // Sets up OpenGL state (binding buffers etc.) such that the scene can be
  // drawn.
  void SetGlState();

  // Actually draws the scene.
  void Draw(std::chrono::system_clock::time_point);

 private:
  void LoadTextures();
  void SetUpShaders();
  void LoadMesh();

  date::month planet_month_;
  gl::Texture2d planet_;

  gl::Texture2d clouds_;

  gl::ShaderProgram program_;
  int uniform_mvp_;
  int uniform_planet_;
  int uniform_clouds_;
  int uniform_sun_direction_;

  Eigen::Matrix4f mvp_;

  gl::VertexArray vao_;

  int width_;
  int height_;
  bool window_size_changed_;
};

}  // namespace glplanet

#endif  // GLPLANET_SRC_SCENE_H_
