// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#ifndef GLPLANET_SRC_UTIL_H_
#define GLPLANET_SRC_UTIL_H_

#include <assert.h>

#include "third_party/abseil/absl/meta/type_traits.h"

// A variant of assert that always evaluates its arguments, even when assertions
// are disabled.
#ifdef NDEBUG
#define DCHECK(x) (void)(x)
#else
#define DCHECK(x) assert(x)
#endif

template <typename T>
constexpr absl::underlying_type_t<T> FromEnum(T x) {
  return static_cast<absl::underlying_type_t<T>>(x);
}

#endif  // GLPLANET_SRC_GL_UTIL_H_
