// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/gl/vertex_array.h"

#include <stdint.h>

#include "src/gl/error.h"
#include "src/util.h"
#include "third_party/abseil/absl/strings/str_cat.h"
#include "third_party/glew/include/GL/glew.h"

namespace gl {

using ::gl_internal::CheckThreadSafety;
using ::gl_internal::ErrorCheck;
using ::gl_internal::UnnecessaryErrorCheck;

void VertexArray::SetVertexAttributeFormat(int index, int size,
                                           VertexAttributeType type, int offset,
                                           bool normalized) {
  CheckThreadSafety();

  glEnableVertexArrayAttrib(array_, index);
  // An error check is necessary here because the index might be too large for
  // OpenGL to handle, but it can be folded into a future error check.
  UnnecessaryErrorCheck();

  switch (type) {
    case VertexAttributeType::kHalfFloat:
    case VertexAttributeType::kFloat:
      glVertexArrayAttribFormat(array_, index, size, FromEnum(type), normalized,
                                offset);
      break;
    case VertexAttributeType::kByte:
    case VertexAttributeType::kShort:
    case VertexAttributeType::kInt:
    case VertexAttributeType::kFixed:
    case VertexAttributeType::kUnsignedByte:
    case VertexAttributeType::kUnsignedShort:
    case VertexAttributeType::kUnsignedInt:
    case VertexAttributeType::kInt2x10x10x10Rev:
    case VertexAttributeType::kUnsignedInt2x10x10x10Rev:
    case VertexAttributeType::kUnsignedInt10f11f11fRev:
      glVertexArrayAttribIFormat(array_, index, size, FromEnum(type), offset);
      break;
    case VertexAttributeType::kDouble:
      glVertexArrayAttribLFormat(array_, index, size, FromEnum(type), offset);
      break;
    default:
      throw std::invalid_argument(
          absl::StrCat("GL: invalid VertexAttributeType ", type));
  }
  // This error check is necessary because a wide variety of argument
  // combinations are invalid.
  ErrorCheck();

  glVertexArrayAttribBinding(array_, index, /*bindingindex=*/0);
  UnnecessaryErrorCheck();
}

}  // namespace gl
