// Copyright 2021, 2022 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/gl/draw.h"

#include <assert.h>

#include "src/gl/error.h"
#include "src/gl/vertex_array.h"
#include "src/util.h"
#include "third_party/glew/include/GL/glew.h"

namespace gl {

void DrawElements(const gl::VertexArray& vao, Primitive mode) {
  gl_internal::CheckThreadSafety();

  GLenum type;
  switch (vao.element_buffer().element_size()) {
    case 1:
      type = GL_UNSIGNED_BYTE;
      break;
    case 2:
      type = GL_UNSIGNED_SHORT;
      break;
    case 4:
      type = GL_UNSIGNED_INT;
      break;
    default:
      assert(false);
  };

  glDrawElements(FromEnum(mode), vao.element_buffer().size(), type,
                 /*indices=*/0);
  // This error check is necessary because the mode could be unsupported.
  gl_internal::ErrorCheck();
}

void SetViewport(int x, int y, int width, int height) {
  gl_internal::CheckThreadSafety();
  glViewport(x, y, width, height);
  gl_internal::ErrorCheck();
}

}  // namespace gl
