// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// Error handling and thread-safety checking.

#ifndef GLPLANET_SRC_GL_ERROR_H_
#define GLPLANET_SRC_GL_ERROR_H_

namespace gl_internal {

#ifdef GLPLANET_DISABLE_GL_THREAD_SAFETY_CHECKS
constexpr bool kThreadSafetyChecks = false;
#else
constexpr bool kThreadSafetyChecks = true;
#endif

#ifdef GLPLANET_DISABLE_AGGRESSIVE_ERROR_CHECKING
constexpr bool kAggressiveErrorChecking = false;
#else
constexpr bool kAggressiveErrorChecking = true;
#endif

#ifndef GLPLANET_DISABLE_GL_THREAD_SAFETY_CHECKS

void RecordThreadImpl();

void CheckThreadSafetyImpl();

#endif

// Records the calling thread as the OpenGL thread.
inline void RecordThread() noexcept(!kThreadSafetyChecks) {
#ifndef GLPLANET_DISABLE_GL_THREAD_SAFETY_CHECKS
  RecordThreadImpl();
#endif
}

// Throws std::logic_error if the current thread is not the OpenGL thread.
inline void CheckThreadSafety() noexcept(!kThreadSafetyChecks) {
#ifndef GLPLANET_DISABLE_GL_THREAD_SAFETY_CHECKS
  CheckThreadSafetyImpl();
#endif
}

// Checks the GL state for errors and throws if an error is present.
void ErrorCheck();

// As ErrorCheck, but disabled by GLPLANET_DISABLE_AGGRESSIVE_ERROR_CHECKING.
inline void UnnecessaryErrorCheck() noexcept(!kAggressiveErrorChecking) {
  if constexpr (kAggressiveErrorChecking) {
    ErrorCheck();
  }
}

}  // namespace gl_internal

#endif  // GLPLANET_SRC_GL_ERROR_H_
