// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// OpenGL initialization.

#ifndef GLPLANET_SRC_GL_INIT_H_
#define GLPLANET_SRC_GL_INIT_H_

#include "src/gl/error.h"
#include "src/util.h"
#include "third_party/glew/include/GL/glew.h"

namespace gl {

// OpenGL capabilities
enum class Capability : GLenum {
  kBlend = GL_BLEND,
  kClipDistance0 = GL_CLIP_DISTANCE0,
  kClipDistance1 = GL_CLIP_DISTANCE1,
  kClipDistance2 = GL_CLIP_DISTANCE2,
  kClipDistance3 = GL_CLIP_DISTANCE3,
  kClipDistance4 = GL_CLIP_DISTANCE4,
  kClipDistance5 = GL_CLIP_DISTANCE5,
  kColorLogicOp = GL_COLOR_LOGIC_OP,
  kCullFace = GL_CULL_FACE,
  kDepthClamp = GL_DEPTH_CLAMP,
  kDepthTest = GL_DEPTH_TEST,
  kDither = GL_DITHER,
  kFramebufferSrgb = GL_FRAMEBUFFER_SRGB,
  kLineSmooth = GL_LINE_SMOOTH,
  kMultisample = GL_MULTISAMPLE,
  kPolygonOffsetFill = GL_POLYGON_OFFSET_FILL,
  kPolygonOffsetLine = GL_POLYGON_OFFSET_LINE,
  kPolygonOffsetPoint = GL_POLYGON_OFFSET_POINT,
  kPolygonSmooth = GL_POLYGON_SMOOTH,
  kRasterizerDiscard = GL_RASTERIZER_DISCARD,
  kSampleAlphaToCoverage = GL_SAMPLE_ALPHA_TO_COVERAGE,
  kSampleAlphaToOne = GL_SAMPLE_ALPHA_TO_ONE,
  kSampleCoverage = GL_SAMPLE_COVERAGE,
  kSampleShading = GL_SAMPLE_SHADING,
  kSampleMask = GL_SAMPLE_MASK,
  kScissorTest = GL_SCISSOR_TEST,
  kStencilTest = GL_STENCIL_TEST,
  kProgramPointSize = GL_PROGRAM_POINT_SIZE,

  // Added in GL 3.1
  kPrimitiveRestart = GL_PRIMITIVE_RESTART,

  // Added in GL 3.2
  kTextureCubeMapSeamless = GL_TEXTURE_CUBE_MAP_SEAMLESS,
};

// Initializes the OpenGL state machine in the current thread. The current
// thread must have an OpenGL context bound.
//
// You must call this function exactly once in your program. Once you do so, you
// may not call any other function in this namespace from any other thread.
void InitializeForThisThread();

// Enables the specified OpenGL capability.
inline void Enable(Capability cap) noexcept(
    !(gl_internal::kThreadSafetyChecks ||
      gl_internal::kAggressiveErrorChecking)) {
  gl_internal::CheckThreadSafety();
  glEnable(FromEnum(cap));
  gl_internal::UnnecessaryErrorCheck();
}

// Sets the clear color for the active framebuffer. Color coordinates should be
// in the range [0, 1].
void SetClearColor(float red, float green, float blue, float alpha);

}  // namespace gl

#endif  // GLPLANET_SRC_GL_INIT_H_
