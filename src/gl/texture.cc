// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/gl/texture.h"

#include <stdexcept>

#include "src/gl/error.h"
#include "src/util.h"
#include "third_party/abseil/absl/strings/str_cat.h"
#include "third_party/glew/include/GL/glew.h"

namespace gl {

void Texture2d::LoadSubimage(int width, int height, PixelFormat format,
                             PixelType type, const void* pixels, int level) {
  gl_internal::CheckThreadSafety();

  if (level >= levels_) {
    throw std::invalid_argument(
        absl::StrCat("GL: invalid texture subimage level $0; maximum is $1",
                     level, levels_ - 1));
  }

  glTextureSubImage2D(texture_, level, /*xoffset=*/0, /*yoffset=*/0, width,
                      height, FromEnum(format), FromEnum(type), pixels);
  // This error check is necessary for a variety of reasons, including a
  // type/format mismatch.
  gl_internal::ErrorCheck();
}

}  // namespace gl
